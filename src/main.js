import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// Register service worker (for PWA support).
//
// TODO: Enable (import) the service worker when the app is relatively
// feature-complete. The service worker will allow the app to be stored offline
// and therefore load quicker (and feel like a true mobile app), but this means
// that updates to the app will be fetched less frequently (vs. a webpage, where
// the latest version is loaded every time).
//
// import "./registerServiceWorker";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app");

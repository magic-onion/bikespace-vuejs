// NOTE: To add a new survey page, add an import statement below, and update
// the `pages` variable.

import Issue from "./Issue";
import Comment from "./Comment";
import Summary from "./Summary";
import Location from "./Location";
import Time from "./Time";
import Photo from "./Photo";

const pages = { Issue, Comment, Summary, Location, Time, Photo };

// The result of lowercasing and postifixing '-page' to the key of every element
// in pages.
//
// For instance, components["issue-page"] == Issue, and so forth.
export const components = Object.entries(pages).reduce((obj, [key, value]) => {
  obj[`${key.toLowerCase()}-page`] = value;
  return obj;
}, {});

# bikespace

_An app that tracks problems with city bike parking spaces._

[![Netlify: Status][netlify-img]][netlify]

## Deployments

| Production                                     | Staging                                                  |
| ---------------------------------------------- | -------------------------------------------------------- |
| [`app.bikespace.ca`](https://app.bikespace.ca) | [`bikespace.netlify.com`](https://bikespace.netlify.com) |

## Project setup

First, install [`yarn`](https://yarnpkg.com/lang/en/docs/install/) for your machine.

Then, use the following commands:

```bash
yarn install   # install dependencies
yarn run serve # compiles and hot-reloads for development
yarn run build # compiles and minifies for production
yarn run test  # Run your tests
yarn run lint # lints and fixes files
```

## CI / CD

This repository is configured to deploy branch `master` to
[Netlify](https://app.netlify.com/sites/bikespace/deploys).

[netlify]: https://app.netlify.com/sites/bikespace/deploys
[netlify-img]: https://api.netlify.com/api/v1/badges/7f5eb0ac-b9e5-4fef-9a7c-84debc111f81/deploy-status
